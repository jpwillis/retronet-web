<HTML>
<HEAD>
    <TITLE>RetroNet</TITLE>
</HEAD>
<BODY BGCOLOR="#FFFFCC">
    
    <H1>RetroNet</H1>
    <HR WIDTH=800 ALIGN=LEFT>
  

    <TABLE WIDTH=800 BORDER=0>
        <TR>
            <TD>
                <P><STRONG>RetroNet</STRONG> is a network where retrocomputing enthusiasts can connect their vintage machines together across the Internet in a safe, isolated environment that supports the tunneling and abstraction of unique, interesting, and obscure protocols.</P>
                <cfif NOT session.loggedIn>
                    <TABLE BORDER=0 CELLPADDING=5>
                        <TR>
                            <TD VALIGN=TOP WIDTH=50%>
                                <cfinclude template="fragments/login.cfm">
                            </TD>
                            <TD VALIGN=TOP WIDTH=50%>
                                <cfinclude template="fragments/register.cfm">
                            </TD>
                        </TR>
                    </TABLE>
                <cfelse>
                    Logged in.
                </cfif>
            </TD>
        </TR>
    </TABLE>

    <HR WIDTH=800 ALIGN=LEFT>

    <P><EM>Copyright &copy; 2018 RetroNet Contributors</EM></P>

</BODY>
</HTML>