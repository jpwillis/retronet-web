<HTML>
<HEAD>
    <TITLE>Registration Status</TITLE>
</HEAD>
<BODY BGCOLOR="#FFFFCC">
    <H1>Registration Status</H1>
    <HR WIDTH=800 ALIGN=LEFT>

    <cftry>
        <cfif form.VOODOO NEQ "green">
            <cfthrow errorcode="RE_BOTCHK" message="Bot check failure.">
        </cfif>

        <cfif form.password NEQ form.passwordconfirm>
            <cfthrow errorcode="RE_PWMISMATCH" message="Passwords do not match">
        </cfif>

        <cfscript>
           acct = new RetroNet.Account(form.username, true);

           acct.username = form.username;
           acct.password = form.password;
           acct.realname = form.realname;
           acct.email = form.email;

           acct.save();
        </cfscript>

        <H1>Account Created.</H1>


        <cfcatch type="any">
            <cfoutput><H1><FONT COLOR=RED>#cfcatch.message#</FONT></H1></cfoutput>
            <a href="default.cfm">Back</a>
        </cfcatch>
    </cftry>
</BODY>
</HTML>